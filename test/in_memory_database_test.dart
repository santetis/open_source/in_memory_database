import 'package:database_interface/database_interface.dart';
import 'package:in_memory_database/in_memory_database.dart';
import 'package:test/test.dart';

void main() {
  group('in memory database', () {
    NoSqlDatabaseInterface database;

    setUpAll(() async {
      database = InMemoryDatabase();
    });

    test('DatabaseIdInterface', () {
      final id = DatabaseId('12');
      expect(id.toString(), 'DatabaseIdInterface("12")');
    });

    test('get collection without opening db', () async {
      try {
        database.collection('test');
      } on DatabaseException catch (e) {
        expect(e, isException);
        expect(e.message, 'Need to open the database first');
      }
    });

    test('drop', () async {
      await database.open();
      final collection = database.collection('drop');
      for (var i = 0; i < 100; i++) {
        await collection.insert(<String, dynamic>{'counter': i});
      }
      await collection.drop();
      final results = await collection.find().toList();
      expect(results, isEmpty);
      await database.close();
    });

    test('isOpen', () async {
      expect(database.isOpen, isFalse);
      await database.open();
      expect(database.isOpen, isTrue);
      await database.close();
      expect(database.isOpen, isFalse);
    });

    test('empty collection', () async {
      await database.open();
      final collection = database.collection('test');
      final results = await collection.find().toList();
      expect(results, isEmpty);
    });

    test('insert', () async {
      final collection = database.collection('test');
      var results = await collection.find().toList();
      expect(results, isEmpty);
      await collection.insert(<String, dynamic>{'name': 'kleak'});
      results = await collection.find().toList();
      expect(results.length, 1);
      expect(results.first.containsKey('name'), isTrue);
      expect(results.first['name'], 'kleak');
      expect((results.first['_id'] as DatabaseId).id, '0');
    });

    test('find', () async {
      final collection = database.collection('test');
      final results =
          await collection.find(<String, dynamic>{'_id': '0'}).toList();
      expect(results.length, 1);
      expect(results.first.containsKey('name'), isTrue);
      expect(results.first['name'], 'kleak');
      expect((results.first['_id'] as DatabaseId).id, '0');
    });

    test('update', () async {
      final collection = database.collection('test');
      await collection.update(<String, dynamic>{'_id': DatabaseId('0')},
          <String, String>{'name': 'titi'});
      var results = await collection.find().toList();
      expect(results.length, 1);
      expect(results.first.containsKey('name'), isTrue);
      expect(results.first['name'], 'titi');
      expect((results.first['_id'] as DatabaseId).id, '0');

      await collection.update(
          <String, String>{'_id': '0'}, <String, dynamic>{'name': 'toto'});
      results = await collection.find().toList();
      expect(results.length, 1);
      expect(results.first.containsKey('name'), isTrue);
      expect(results.first['name'], 'toto');
      expect((results.first['_id'] as DatabaseId).id, '0');

      await collection.update(
          <String, String>{'_id': '1000'}, <String, dynamic>{'name': 'kleak'});
      results = await collection.find().toList();
      expect(results.length, 1);
      expect(results.first.containsKey('name'), isTrue);
      expect(results.first['name'], 'toto');
      expect((results.first['_id'] as DatabaseId).id, '0');
    });

    test('findOne', () async {
      final collection = database.collection('test');
      var result = await collection.findOne(<String, dynamic>{'_id': '0'});
      expect(result, isNotNull);
      expect(result['name'], 'toto');
      expect((result['_id'] as DatabaseId).id, '0');

      result =
          await collection.findOne(<String, dynamic>{'_id': DatabaseId('0')});
      expect(result, isNotNull);
      expect(result['name'], 'toto');
      expect((result['_id'] as DatabaseId).id, '0');

      result = await collection.findOne(<String, dynamic>{'_id': '1000'});
      expect(result, isNull);
    });

    test('delete', () async {
      final collection = database.collection('test');
      await collection.delete(<String, dynamic>{'_id': DatabaseId('0')});
      final results = await collection.find().toList();
      expect(results, isEmpty);
    });
  });
}
