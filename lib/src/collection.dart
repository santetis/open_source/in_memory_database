import 'dart:async';

import 'package:database_interface/database_interface.dart';

class InMemoryCollection implements CollectionInterface {
  int _id = 0;
  final collection = <Map<String, dynamic>>[];

  @override
  Future<void> delete(Map<String, dynamic> selector) async {
    final index = collection.indexWhere((doc) {
      var match = true;
      for (final key in selector.keys) {
        if (doc[key] != selector[key]) {
          match = false;
        }
      }
      return match;
    });
    if (index != -1) {
      collection[index] = null;
    }
  }

  @override
  Future<void> drop() async {
    collection.clear();
  }

  @override
  Stream<Map<String, dynamic>> find([Map<String, dynamic> selector]) {
    if (selector != null) {
      return Stream.fromIterable(collection.where((doc) {
        if (doc == null) {
          return false;
        }
        var match = true;
        for (final key in selector.keys) {
          if (doc[key] != selector[key]) {
            match = false;
          }
        }
        return match;
      }));
    }
    return Stream.fromIterable(collection.where((doc) => doc != null));
  }

  @override
  Future<Map<String, dynamic>> findOne(Map<String, dynamic> selector) async =>
      collection.firstWhere((doc) {
        if (doc == null) {
          return false;
        }
        var match = true;
        for (final key in selector.keys) {
          if (doc[key] != selector[key]) {
            match = false;
          }
        }
        return match;
      }, orElse: () => null);

  @override
  Future<void> insert(Map<String, dynamic> document) async {
    document['_id'] = DatabaseId('$_id');
    _id++;
    collection.add(document);
  }

  @override
  Future<void> update(
      Map<String, dynamic> selector, Map<String, dynamic> document) async {
    final index = collection.indexWhere((doc) {
      if (doc == null) {
        return false;
      }
      var match = true;
      for (final key in selector.keys) {
        if (doc[key] != selector[key]) {
          match = false;
        }
      }
      return match;
    });
    if (index != -1) {
      final newDocument = collection[index];
      for (final key in document.keys) {
        newDocument[key] = document[key];
      }
      collection[index] = newDocument;
    }
  }
}
